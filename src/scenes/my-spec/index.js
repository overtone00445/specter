import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Alert,
  FlatList,
  LayoutAnimation,
  TouchableOpacity
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

import { Actions } from "react-native-router-flux";

import Button from '../../components/custom-button'
import FavoriteSpecBox from '../../components/favorite-spec-box'

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import AppText from '../../components/app-text';
import TitleIndicator from "../../components/title-indicator";
import Modal from "react-native-modal";

const mapStateToProps = (state) => {
  return {
    favoriteSpecs: state.favoriteSpecs
  }
}

class MySpec extends Component {

  _navigateToSpecDetail(item, specIdx) {
    Actions.push('Favorite-spec-detail', {
      spec: item,
      specIdx
    });
  }

  _renderFavoriteSpec() {
    const favoriteSpecs = this.props.favoriteSpecs;

    if (favoriteSpecs != null && favoriteSpecs.length > 0) {
      return (
        <FlatList
          contentContainerStyle={global.pageFlatList}
          data={favoriteSpecs}
          renderItem={({ item }) => {
            return (
              <FavoriteSpecBox
                name={item.name}
                price={item.price + ' THB'}
                mode={item.mode}
                date={item.date}
                onPress={() => this._navigateToSpecDetail(item, favoriteSpecs.indexOf(item))}>
              </FavoriteSpecBox>);
          }}
          keyExtractor={(item) => favoriteSpecs.indexOf(item)}
        />
      );
    } else {
      return (
        <View style={local.info}>
          <AppText size="l" value="There is no favorite spec" />
        </View>
      );
    }
  }


  render() {
    return (
      <View style={global.pageContainer}>
        {this._renderFavoriteSpec()}
      </View>
    );
  }
}

export default connect(mapStateToProps)(MySpec);