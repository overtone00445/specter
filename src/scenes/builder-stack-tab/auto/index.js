import React, { Component } from 'react';
import {
  View,
  ScrollView,
  LayoutAnimation,
  Picker,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';

import Spinner from "react-native-spinkit";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  getAutoRecommend,
  getAutoPartialRecommend
} from "../../../actions/request-api";
import { loadFavoriteSpecs } from "../../../actions/favorite-specs";

import { local } from "./style";
import { colors, global, invertColor } from "../../../theme";

// Container component
import BudgetForm from "../../../containers/budget-form";
import RecommendList from "../../../containers/recommend-list";

// Custom component
import AppText from "../../../components/app-text";
import Button from "../../../components/custom-button";
import TitleIndicator from "../../../components/title-indicator";
import CheckBox from 'react-native-check-box'
import Modal from "react-native-modal";
import { SegmentedControls } from 'react-native-radio-buttons'

import { hook } from 'cavy';


const mapStateToProps = (state) => {
  return {
    service: state.service,
    recommends: state.recommends,
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getAutoRecommend: getAutoRecommend,
    getAutoPartialRecommend: getAutoPartialRecommend,
    loadFavoriteSpecs: loadFavoriteSpecs,
  }, dispatch);
}


class Auto extends Component {
  state = {
    pcType: 'gaming',
    budgetValue: "25000",
    showModal: false,
    selectedParts: [
      { "CPU": false },
      { "Mainboard": true },
      { "GPU": true },
      { "Harddisk": true },
      { "SSD": false },
      { "RAM": false },
      { "PowerSupply": false },
      { "Monitor": false }
    ],
    partialOptions: ['Gaming', 'Designer'],
    selectedPartialOption: 'Gaming',
    pcTypeOfPartial: 'gaming',
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loadFavoriteSpecs();
  }

  componentWillReceiveProps() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  _generateSpec() {
    const isInvalid = (this.state.budgetValue == ''
      || isNaN(this.state.budgetValue)
      || Number.parseInt(this.state.budgetValue) <= 0);

    if (isInvalid == true) {
      alert('Invalid input');
      return;
    } else {
      const budget = parseInt(this.state.budgetValue);
      if (this.state.pcType != 'partial') {
        this.props.getAutoRecommend(budget, this.state.pcType);
      } else {
        this.props.getAutoPartialRecommend(budget, this.state.selectedParts, this.state.pcTypeOfPartial);
      }
    }
  }

  _setPCType(value) {
    this.setState({ pcType: value });

    if (value == "partial") {
      this.setState({ showModal: true });
    }
  }

  _setPcTypeForPartial(value) {
    const pcType = value == 'Gaming' ? 'gaming' : 'designer';
    this.setState({ pcTypeOfPartial: pcType, selectedPartialOption: value });
  }

  _renderRecommendList() {
    const recommendSpecs = this.props.recommends.auto;

    if (Object.keys(recommendSpecs).length > 0) {
      return <RecommendList noBorder marginX mode="Auto" invert recommendSpecs={recommendSpecs} />
    }
  }

  _setModalVisible(value) {
    this.setState({
      showModal: value
    });
  }

  _selectComponent(index, key) {
    const selectedParts = this.state.selectedParts;
    selectedParts[index][key] = !selectedParts[index][key];

    this.setState({ selectedParts });
  }

  _renderModal() {
    return (
      <Modal
        animationIn="fadeIn"
        animationOut="fadeOut"
        isVisible={this.state.showModal}
        onBackButtonPress={() => this._setModalVisible(false)}
        onBackdropPress={() => this._setModalVisible(false)}>
        <View style={global.modalContainer}>
          <View style={local.modalTitle}>
            <TitleIndicator noBorder value="Select Parts" />
          </View>
          <FlatList
            data={this.state.selectedParts}
            renderItem={({ item }) => {
              let itemIndex = this.state.selectedParts.indexOf(item);
              let itemKey = Object.keys(item)[0];
              let itemValue = item[itemKey];
              return (
                <CheckBox
                  style={{ flex: 1, padding: 10 }}
                  checkBoxColor={colors.greenD}
                  onClick={() => { this._selectComponent(itemIndex, itemKey) }}
                  isChecked={itemValue}
                  leftText={itemKey}
                />);
            }}
            keyExtractor={(item) => this.state.selectedParts.indexOf(item)}
          />
          <View style={{ padding: 10, borderTopColor: colors.whiteE }}>
            <SegmentedControls
              optionStyle={{ fontWeight: 'normal', fontFamily: 'Kanit-SemiBold' }}
              tint={colors.greenC}
              backTint={colors.whiteC}
              selectedTint={colors.whiteA}
              options={this.state.partialOptions}
              containerBorderWidth={1}
              onSelection={(value) => this._setPcTypeForPartial(value)}
              selectedOption={this.state.selectedPartialOption}
              renderOption={(option, selected) => {
                return (
                  <View ref={option} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <AppText value={option} color={selected == true ? colors.whiteA : null} />
                  </View>
                );
              }}
            />
          </View>
        </View>
      </Modal>
    );
  }

  _renderSearchButton() {
    if (this.props.service.isLoading) {
      return (
        <Spinner
          isVisible={this.props.service.isLoading}
          style={local.spinner}
          type={"Bounce"}
          size={60}
          color={colors.whiteB} />
      );
    }

    return (
      <View style={local.searchBtnContainer}>
        <TouchableOpacity activeOpacity={0.5} style={local.searchBtn} onPress={() => { this._generateSpec() }} ref={this.props.generateTestHook('Auto.Search')}>
          <Image style={local.searchIcon} source={require('../../../assets/icons/search.png')}></Image>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <View style={[global.pageContainer, local.pageContainer]}>
        {this._renderModal()}
        <ScrollView contentContainerStyle={global.pageScrollView} >
          <View style={local.budgetFormContainer}>
            <View style={local.formTitle}>
              <AppText value="Preference" size="xxxl" bold color={colors.whiteB} />
            </View>

            <View style={local.formBody}>
              <View style={local.inputTitle}>
                <AppText size="sm" value="PC Type" color={colors.whiteB} />
              </View>
              <Image style={local.arrowDown}
                source={require('../../../assets/icons/arrow-down.png')} />
              <View style={local.roundBox}>
                <Picker
                  style={local.picker}
                  mode="dropdown"
                  selectedValue={this.state.pcType}
                  onValueChange={(itemValue, itemIndex) => this._setPCType(itemValue)}>
                  <Picker.Item label="Gaming" value="gaming" />
                  <Picker.Item label="Designer" value="designer" />
                  <Picker.Item label="Partial" value="partial" />
                </Picker>
              </View>

              <View style={local.inputTitle} accessibilityLabel = "testview">
                <AppText size="sm" value="Budget (THB)" color={colors.whiteB} />
              </View>
              <View style={[local.roundBox, { marginBottom: 22 }]}>
                <TextInput
                  accessibilityLabel = "moneyInput"
                  ref={this.props.generateTestHook('Auto.TextInput')}
                  keyboardType="numeric"
                  style={local.textInput}
                  value={this.state.budgetValue}
                  selectionColor={colors.greenB}
                  underlineColorAndroid='transparent'
                  placeholder="25000"
                  onSubmitEditing={(value) => { this._generateSpec() }}
                  placeholderTextColor={colors.whiteE}
                  onChangeText={(value) => this.setState({ budgetValue: value })} />
              </View>


              {this._renderSearchButton()}


            </View>
          </View>

          {this._renderRecommendList()}

        </ScrollView>
      </View >
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(Auto);
const TestAuto = hook(connect(mapStateToProps, mapDispatchToProps)(Auto));
export default TestAuto;