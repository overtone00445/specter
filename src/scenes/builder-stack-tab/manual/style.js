import { StyleSheet } from "react-native";

export const local = StyleSheet.create({
  budgetFormContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 5
  },
});
