import React, { Component } from 'react';
import {
  View,
  ScrollView,
  FlatList,
  ToastAndroid,
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addToMySpec } from "../../actions/my-spec";
import { requestComponent } from "../../actions/request-api";
import { changeComponent } from "../../actions/favorite-specs";
import {
  sortProductNameASC,
  sortProductNameDES,
  sortProductPriceASC,
  sortProductPriceDES,
} from "../../actions/product-list";


import Spinner from "react-native-spinkit";

import ProductFilter from "../../components/product-filter";
import ProductItem from "../../components/product-item";

import { hook } from 'cavy';

const mapStateToProps = (state) => {
  return {
    service: state.service,
    products: state.products,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    requestComponent: requestComponent,
    changeComponent: changeComponent,
    addToMySpec: addToMySpec,
    sortNameASC: sortProductNameASC,
    sortNameDES: sortProductNameDES,
    sortPriceASC: sortProductPriceASC,
    sortPriceDES: sortProductPriceDES,
  }, dispatch);
}

class ProductList extends Component {
  state = {
    sortType: 'Price',
  }

  componentDidMount() {
    // Request the list of products
    this.props.requestComponent(this.props.requestName);
  }

  _navigateToDetail(data) {
    data.ComponentType = this.props.title; // Attach the type of component
    data.requestName = this.props.requestName; // To edit the spec
    Actions.push('Product-detail', {
      product: data,
      showAddBtn: true,
    })
  }

  _addToMySpec(item) {
    item.ComponentType = this.props.title; // Attach the type of component
    item.requestName = this.props.requestName; // To edit the spec

    if (this.props.addMode == null) {
      this.props.addToMySpec(item);
      ToastAndroid.show('Add to my spec', ToastAndroid.SHORT);
      // Actions.reset('Builder-stack-tab');
      // Actions.jump('Manual');
      Actions.pop();

    } else if (this.props.addMode == 'changeComp') {
      const { specIdx, componentIdx } = this.props;
      this.props.changeComponent(specIdx, componentIdx, item);
      ToastAndroid.show('Change a component', ToastAndroid.SHORT);
      Actions.pop(); Actions.pop(); // To fix a nonsense bug
      Actions.push('Favorite-spec-detail', { specIdx });
    }
  }

  _changeSortingType(type, order) {
    const products = this.props.products;
    const componentType = this.props.requestName;
    if (type == 'Name') {
      if (order == 'Ascending') {
        this.props.sortNameASC(products, componentType);
      } else {
        this.props.sortNameDES(products, componentType);
      }
    } else {
      if (order == 'Ascending') {
        this.props.sortPriceASC(products, componentType);
      } else {
        this.props.sortPriceDES(products, componentType);
      }
    }
    ToastAndroid.show(
      'Sorted by ' + type,
      ToastAndroid.SHORT);
  }

  _renderProducts() {
    if (this.props.service.isLoading == false) {
      const products = this.props.products[this.props.requestName].products;
      return (
        <FlatList
          contentContainerStyle={global.pageFlatList}
          ListHeaderComponent={
            <ProductFilter
              sortType={this.state.sortType}
              onChange={(type, order) => { this._changeSortingType(type, order) }} />}
          data={products}
          renderItem={({ item }) =>
            <ProductItem

              showAddBtn
              name={item.Title}
              price={item.Price}
              image={item.ImgURL}
              onPress={() => this._navigateToDetail(item)}
              onAdd={() => this._addToMySpec(item)}>
            </ProductItem>}
          keyExtractor={item => item.Title}
        />
      );
    }
  }

  render() {
    return (
      <View style={[global.pageContainer]}>

        <Spinner
          isVisible={this.props.service.isLoading}
          style={local.spinner}
          type={"Wave"}
          size={50}
          color={colors.greenB} />

        {this._renderProducts()}

      </View >
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(ProductList);

const TestProductList = hook(connect(mapStateToProps, mapDispatchToProps)(ProductList));
export default TestProductList;