import { StyleSheet } from "react-native";

import { colors } from "../../theme";

export const local = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.greenD,
  },
  slide1: {
    flex: 1,
    alignItems: 'center',
    padding: 5
  },
  imageContainer: {
    flex: 4,
    justifyContent: 'center',
    paddingVertical: 20,
  },
  image: {
    flex: 1,
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  description: {
    flex: 3,
    padding: 10,
    justifyContent: 'center',
    transform: [
      { translateY: -60}
    ]
  }
});