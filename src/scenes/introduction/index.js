import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  AsyncStorage
} from 'react-native';

import AppIntro from 'react-native-app-intro';

import { local } from "./style";
import { colors, global } from "../../theme";

import { Actions } from "react-native-router-flux";

import AppText from '../../components/app-text';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const animOffset = 10;

class Intro extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showSlides: false
    }
  }

  _setSlidesVisible(visible){
    this.setState({
      showSlides: visible
    });
  }

  componentDidMount() {
    AsyncStorage.getItem('isIntroFirstTime', (err, result) => {
      if (err) {
      } else {
        if (result == null) { // load first times
          this._setSlidesVisible(true);
        } else { // second times
          Actions.reset('Builder-stack-tab');
        }
      }
    });
  }

  _navigateToAutoScene() {
    Actions.jump('Builder-stack-tab')
    AsyncStorage.setItem('isIntroFirstTime', JSON.stringify(false)); // loadFirstTime = false
  }

  _renderIntroSlides() {
    if (this.state.showSlides == true) {
      return (
        <AppIntro
          customStyles={{ btnContainer: {flex: 1} }}
          onDoneBtnClick={() => { this._navigateToAutoScene() }}
          onSkipBtnClick={() => { this._navigateToAutoScene() }}
        >
          <View style={local.slide}>
            <View accessibilityLabel='appintro'><AppText bold size="xxxl" value="Welcome" color={colors.whiteA} /></View>
            <AppText size="xl" value="This is an application" color={colors.whiteA} />
            <AppText size="xl" value="for building your own" color={colors.whiteA} />
            <AppText size="xl" value="custom computer" color={colors.whiteA} />
          </View>

          <View style={local.slide}>
            <View style={local.imageContainer} level={animOffset}>
              <Image resizeMethod="resize" style={local.image} source={require('../../assets/images/intro-1.png')} />
            </View>
            <View style={local.description} level={animOffset} >
              <AppText size="l" center={true} color={colors.whiteA} value="Auto Mode: choose your preference and put your budget,
              application will generate the best spec for you" numLine={5}/>
            </View>
          </View>

          <View style={local.slide}>
            <View style={local.imageContainer} level={animOffset}>
              <Image resizeMethod="resize" style={local.image} source={require('../../assets/images/intro-2.png')} />
            </View>
            <View style={local.description} level={animOffset} >
              <AppText size="l" center={true} color={colors.whiteA} value="Manual mode: choose the component
              by yourself, bulid your own real custom computer spec" numLine={5} />
            </View>
          </View>

          <View style={local.slide}>
            <View style={local.imageContainer} level={animOffset}>
              <Image resizeMethod="resize" style={local.image} source={require('../../assets/images/intro-3.png')} />
            </View>
            <View style={local.description} level={animOffset} >
              <AppText size="l" center={true} color={colors.whiteA} value="My Spec: collect your favorite spec
              from building modes, you can edit some component later, if you want" numLine={5} />
            </View>
          </View>
        </AppIntro>
      );
    }
  }

  render() {
    return (
      <View accessibilityLabel="testview">
        {this._renderIntroSlides()}
      </View>
    );
  }
}

export default Intro;
