import React, { Component } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  Image,
  ToastAndroid,
  TextInput,
  Dimensions
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { removeFromMySpec, clearSpec } from "../../actions/my-spec";
import { addToFavorite } from "../../actions/favorite-specs";

// Custom component
import AppText from "../../components/app-text";
import Button from "../../components/custom-button";
import TitleIndicator from "../../components/title-indicator";
import PreferenceItem from "../../components/preference-item";

import Modal from "react-native-modal";

import { hook } from 'cavy'; 


const mapStateToProps = (state) => {
  return {
    mySpec: state.mySpec,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    removeFromMySpec: removeFromMySpec,
    clearSpec: clearSpec,
    addToFavorite: addToFavorite,
  }, dispatch);
}

class PreferenceList extends Component {
  state = {
    selected: null,
    showDeleteModal: false,
    showInputName: false,
    selectedIndex: null,
    specName: '',
  }

  _openInputModal() {
    this.setState({
      showInputName: true
    })
  }

  _closeModal() {
    this.setState({
      showInputName: false,
      showDeleteModal: false,
    })
  }

  _navigateToDetail(data) {
    Actions.push('Product-detail', {
      product: data,
      showAddBtn: false,
    });
  }

  _confirmDelete() {
    this.props.removeFromMySpec(this.state.selectedIndex);
    this._closeModal();
  }

  _onDelete(idx) {
    this.setState({ showDeleteModal: true, selectedIndex: idx });
  }

  _saveThisSpec() {
    this._closeModal();
    this.props.addToFavorite(
      this.props.mySpec,
      this.state.specName,
      this.props.mode
    ); // Add building spec to favorite

    ToastAndroid.show('Save to favorite', ToastAndroid.SHORT);
    this.props.clearSpec();
  }

  _renderPreferenceItems() {
    const views = [];
    const mySpec = this.props.mySpec.components;

    for (let idx in mySpec) {
      views.push(
        <PreferenceItem
          key={idx}
          name={mySpec[idx].Title}
          image={mySpec[idx].ImgURL}
          price={mySpec[idx].Price}
          onPress={() => { this._navigateToDetail(mySpec[idx]) }}
          onDelete={() => this._onDelete(idx)} />
      );
    }
    return views;
  }

  _renderConfirmDeleteModal() {
    return (
      <Modal
        animationIn="fadeIn"
        animationOut="fadeOut"
        isVisible={this.state.showDeleteModal}
        onBackButtonPress={() => this._closeModal()}
        onBackdropPress={() => this._closeModal()}>
        <View style={global.modalContainer}>
          <View style={local.modalTitle}>
            <TitleIndicator noBorder value="Do you want to remove this component" />
          </View>
          <View style={local.confirmContainer}>
            <TouchableOpacity style={local.confirmBtn} onPress={() => this._confirmDelete()} ref={this.props.generateTestHook('confirmDelete.btn')}>
              <AppText size="sm" value="OK" center bold color={colors.whiteB} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  _renderModal() {
    return (
      <Modal
        animationIn="fadeIn"
        animationOut="fadeOut"
        isVisible={this.state.showInputName}
        onBackButtonPress={() => this._closeModal()}
        onBackdropPress={() => this._closeModal()}>
        <View style={[global.modalContainer]}>
          <View style={local.modalTitle}>
            <TitleIndicator noBorder value="Input a name of your spec" />
          </View>
          <TextInput
            ref={this.props.generateTestHook('Save.textInput')}
            autoFocus
            maxLength={20}
            style={local.textInput}
            value={this.state.specName}
            selectionColor={colors.greenB}
            underlineColorAndroid={colors.greenB}
            placeholder="Optimal spec"
            placeholderTextColor={colors.whiteE}
            onSubmitEditing={(value) => { this._saveThisSpec() }}
            onChangeText={(value) => this.setState({ specName: value })} />
          <View style={local.confirmContainer}>
            <TouchableOpacity style={local.confirmBtn} onPress={() => this._saveThisSpec()} ref={this.props.generateTestHook('Save.textInput.confirmBtn')}>
              <AppText size="sm" value="OK" center bold color={colors.whiteB} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <View style={local.container}>
        {this._renderModal()}
        {this._renderConfirmDeleteModal()}
        <View style={local.titleContainer}>
          <View style={local.titleIndicatorContainer}>
            <TitleIndicator value="Selected Components" />
          </View>
          <View style={{
            justifyContent: 'flex-end'
          }}>
            <TouchableOpacity style={local.saveButton} onPress={() => { this._openInputModal() }} ref={this.props.generateTestHook('Save.btn')}>
              <Image style={local.saveIcon} source={require('../../assets/icons/save.png')}></Image>
              <AppText value="Save" size="sm" bold color={colors.whiteB} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={local.customCard}>
          <ScrollView horizontal contentContainerStyle={local.scrollView}>
            {this._renderPreferenceItems()}
          </ScrollView>
        </View>
      </View>
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(PreferenceList);
const TestPreferenceList = hook(connect(mapStateToProps, mapDispatchToProps)(PreferenceList));
export default TestPreferenceList;