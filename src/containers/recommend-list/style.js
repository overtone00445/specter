import { StyleSheet } from "react-native";
import { colors } from "../../theme";

export const local = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    borderRadius: 4,
    overflow: "hidden",
    padding: 0,
  }
});
