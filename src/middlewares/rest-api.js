import {
  GET_COMPONENT,
  GET_RECOMMEND_FOR_AUTO,
  GET_PARTIAL_FOR_AUTO,
  GET_RECOMMEND_FOR_MANUAL,
} from "../actions/types";

import {
  servicePending,
  serviceError,
  serviceSuccess,
} from "../actions/request-api";

import axios from "react-native-axios";
import { setAutoRecommend, setManualRecommend } from "../actions/recommend-spec";
import { setProductList, sortProductPriceASC } from "../actions/product-list";

// Preset priority of pc type

const gamingType = {
  GPU: [7, {}],
  CPU: [10, {}],
  Mainboard: [5, {}],
  RAM: [4, {}],
  SSD: [3, {}],
  Harddisk: [2, {}],
  PowerSupply: [2, {}],
  Monitor: [1, {}]
};

const designerType = {
  CPU: [7, { Core: 5, Thread: 3 }],
  GPU: [6, {}],
  RAM: [5, {}],
  Mainboard: [4, {}],
  SSD: [3, {}],
  Harddisk: [3, {}],
  Monitor: [2, {}],
  PowerSupply: [3, {}],
}


const manualType = {
  GPU: [1, {}],
  CPU: [1, {}],
  Mainboard: [1, {}],
  RAM: [1, {}],
  SSD: [1, {}],
  Harddisk: [1, {}],
  PowerSupply: [1, {}],
  Monitor: [1, {}]
}


const presetPriority = {
  gaming: gamingType,
  designer: designerType,
  manual: manualType
}

function filterAttributes(object, excludeAttr) {
  for (let attr of excludeAttr) {
    delete object[attr];
  }
}

export const apiService = store => next => action => {
  next(action);
  switch (action.type) {
    case GET_COMPONENT: {
      const products = store.getState().products;
      const componentType = action.payload.componentType;
      if (products[componentType].isLoadFirstTime == true) {
        store.dispatch(servicePending());
        axios.get('https://desolate-peak-95662.herokuapp.com/api/' + componentType)
          .then(res => {
            data = res.data.data;
            store.dispatch(setProductList(data, componentType));
            store.dispatch(sortProductPriceASC(data, componentType));
            store.dispatch(serviceSuccess(null));
          })
          .catch(err => {
            store.dispatch(serviceError(err));
          });
      }
    } break;

    case GET_RECOMMEND_FOR_AUTO: {
      store.dispatch(servicePending());

      const pcType = action.payload.pcType;
      const budget = action.payload.budget;
      const postData = {
        budget: budget,
        priorities: presetPriority[pcType],
        selectedComponents: []
      }

      axios.post('https://desolate-peak-95662.herokuapp.com/api/getBestSpecs', postData)
        .then(res => {
          const bestSpecs = res.data['bestSpecs'];
          store.dispatch(setAutoRecommend(bestSpecs));
          store.dispatch(serviceSuccess(null));
        })
        .catch(
        err => {
          store.dispatch(serviceError(err));
        }
        );

    } break;

    case GET_PARTIAL_FOR_AUTO: {
      store.dispatch(servicePending());
      const pcType = action.payload.pcType;
      const budget = action.payload.budget;
      const priorities = {};

      for (let element of action.payload.selectedParts) {
        let partName = Object.keys(element)[0];
        // if (element[partName] == true) {
        //   priorities[partName] = [1, {}];
        // }
        if (element[partName] == true) {
          priorities[partName] = presetPriority[pcType][partName];
        }
      }

      const postData = {
        budget: budget,
        priorities,
        selectedComponents: []
      }

      axios.post('https://desolate-peak-95662.herokuapp.com/api/getBestSpecs', postData)
        .then(res => {
          const bestSpecs = res.data['bestSpecs'];
          store.dispatch(setAutoRecommend(bestSpecs));
          store.dispatch(serviceSuccess(null));
        })
        .catch(
        err => {
          store.dispatch(serviceError(err));
        }
        );
    } break;

    case GET_RECOMMEND_FOR_MANUAL: {
      store.dispatch(servicePending());
      const selectedComponents = {};
      const pcType = action.payload.pcType;
      const priorities = Object.assign({}, presetPriority[pcType]);
      const budget = action.payload.budget;
      const excludeAttr = ["CartURL", "ComponentType", "ImgURL", "requestName"];


      for (let element of action.payload.specificItems) {
        const { ComponentType } = element;
        // filterAttributes(element, excludeAttr);
        delete priorities[ComponentType];
        selectedComponents[ComponentType] = element;
        // priorities[ComponentType] = presetPriority[pcType][ComponentType];
      }

      const postData = {
        budget,
        priorities,
        selectedComponents
      }

      axios.post('https://desolate-peak-95662.herokuapp.com/api/getBestSpecs', postData)
        .then(res => {
          const bestSpecs = res.data['bestSpecs'];
          store.dispatch(setManualRecommend(bestSpecs));
          store.dispatch(serviceSuccess(null));
        })
        .catch(
        err => {
          store.dispatch(serviceError("err"));
        }
        );
    } break;

    default:
      break;
  }
}