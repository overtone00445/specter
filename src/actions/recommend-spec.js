import {
  SET_RECOMMEND_FOR_AUTO,
  SET_RECOMMEND_FOR_MANUAL,
} from "./types";

export const setAutoRecommend = (specs) => {
  return {
    type: SET_RECOMMEND_FOR_AUTO,
    payload: { specs }
  }
}

export const setManualRecommend = (specs) => {
  return {
    type: SET_RECOMMEND_FOR_MANUAL,
    payload: { specs }
  }
}