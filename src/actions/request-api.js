import {
  GET_COMPONENT,
  SERVICE_PENDING,
  SERVICE_SUCCESS,
  SERVICE_ERROR,
  GET_RECOMMEND_FOR_AUTO,
  GET_RECOMMEND_FOR_MANUAL,
  GET_PARTIAL_FOR_AUTO
} from "./types";

export const requestComponent = (cType) => {
  return {
    type: GET_COMPONENT,
    payload: { componentType: cType }
  }
}

export const getAutoRecommend = (budget, pcType) => {
  return {
    type: GET_RECOMMEND_FOR_AUTO,
    payload: { budget, pcType }
  }
}

export const getAutoPartialRecommend = (budget, selectedParts, pcType) => {
  return {
    type: GET_PARTIAL_FOR_AUTO,
    payload: {
      budget,
      selectedParts,
      pcType
    }
  }
}

export const getManualRecommend = (budget, specificItems, pcType) => {
  return {
    type: GET_RECOMMEND_FOR_MANUAL,
    payload: {
      budget,
      specificItems,
      pcType
    }
  }
}

export const servicePending = () => {
  return {
    type: SERVICE_PENDING
  }
}

export const serviceSuccess = (res) => {
  return {
    type: SERVICE_SUCCESS,
    payload: { response: res }
  }
}

export const serviceError = (err) => {
  return {
    type: SERVICE_ERROR,
    payload: { error: err }
  }
}