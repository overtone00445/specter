import {
  ADD_TO_MY_SPEC,
  REMOVE_FROM_MY_SPEC,
  SET_RECOMMEND_FOR_AUTO,
  SET_RECOMMEND_FOR_MANUAL,
  RESPONSE_GET_AUTO
} from "../actions/types";

const recommends = {
  auto: [],
  manual: [],
};

function attachRequestName(bestSpecs) {
  for (let spec of bestSpecs) {
    for (let element in spec) {
      spec[element].requestName = element.toLowerCase();
      spec[element].ComponentType = element;
    }
  }
}

export default reducerRecommendSpec = (state = recommends, action) => {
  switch (action.type) {
    case SET_RECOMMEND_FOR_AUTO: {
      let bestSpecs = JSON.parse(action.payload.specs);
      const nextState = Object.assign({}, state);
      attachRequestName(bestSpecs);
      nextState.auto = bestSpecs;
      return nextState;
    } break;

    case SET_RECOMMEND_FOR_MANUAL: {
      let bestSpecs = JSON.parse(action.payload.specs);
      const nextState = Object.assign({}, state);
      attachRequestName(bestSpecs);
      nextState.manual = bestSpecs;
      return nextState;
    } break;

    case RESPONSE_GET_AUTO: {

    } break;

    default:
      return state;
  }
}