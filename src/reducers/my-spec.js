import {
  ADD_TO_MY_SPEC,
  REMOVE_FROM_MY_SPEC,
  CLEAR_MY_SPEC
} from "../actions/types";

const mySpec = {
  components: [],
  gpuCount: 0
};

export default reducerMySpec = (state = mySpec, action) => {
  switch (action.type) {
    case ADD_TO_MY_SPEC: {
      const nextState = Object.assign({}, state);
      const components = nextState.components;
      let isExist = false;

      for (let idx in components) {
        if (action.payload.newComp.ComponentType == components[idx].ComponentType) {
          components[idx] = action.payload.newComp;
          isExist = true;
        }
      }

      if (isExist == false) { nextState.components.unshift(action.payload.newComp); } // Make it be stack
      return nextState;
    }
      break;

    case REMOVE_FROM_MY_SPEC: {
      const nextState = Object.assign({}, state);
      nextState.components.splice(action.payload.removeIdx, 1);
      return nextState;
    }
      break;

    case CLEAR_MY_SPEC: {
      let nextState = {
        components: []
      };
      return nextState;
    } break;


    default:
      return state;
  }
}