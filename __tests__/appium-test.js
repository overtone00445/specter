import wd from 'wd';



jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

const PORT = 4723;

const config = {

  platformName: 'Android',

  deviceName: 'Android Emulator',

  app: './android/app/build/outputs/apk/app-debug.apk' // relative to root of project

};

const driver = wd.promiseChainRemote('localhost', PORT);



beforeAll(async () => {

  await driver.init(config);

  await driver.sleep(2000); // wait for app to load

})



test('appium renders', async () => {

  expect(await driver.hasElementByAccessibilityId('testview')).toBe(true);

  // expect(await driver.hasElementByAccessibilityId('notthere')).toBe(false);

  await driver.sleep(2000); // wait for app to load

  // expect(await driver.hasElementByAccessibilityId('appintro')).toBe(true);


});

test('auto page', async () => {

  // expect(await driver.hasElementByAccessibilityId('test')).toBe(true);
  

  // expect(await driver.hasElementByAccessibilityId('notthere')).toBe(false);

  await driver.sleep(2000); // wait for app to load

  // expect(await driver.hasElementByAccessibilityId('pick')).toBe(true);
  // await driver.elementByAccessibilityId('pick').click();
  // expect(await driver.hasElementByAccessibilityId('game')).toBe(true);
  expect(await driver.hasElementByAccessibilityId('moneyInput')).toBe(true);
  await driver.elementByAccessibilityId('moneyInput').type('30000');
  // expect(await driver.hasElementByAccessibilityId('search')).toBe(true);
  // await driver.elementByAccessibilityId('search').click().click();

  

  // expect(await driver.hasElementByAccessibilityId('moneyInputM')).toBe(true);
  // await driver.elementByAccessibilityId('moneyInputM').type('30000');


});
