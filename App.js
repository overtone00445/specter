import React, { Component } from 'react';
import {
  UIManager,
  View,
} from 'react-native';

// Required for LayoutAnimation
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

// Redux Store
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { allReducers } from "./src/reducers";
import { apiService, storageService, socketService } from "./src/middlewares";

// import createSocketIoMiddleware from 'redux-socket.io';
// import io from 'socket.io-client';

// Routing
import { Router, Stack, Scene, Drawer, Tabs } from "react-native-router-flux";

// Scenes
import Auto from "./src/scenes/builder-stack-tab/auto";
import Manual from "./src/scenes/builder-stack-tab/manual";
import MySpec from "./src/scenes/my-spec";
import ProductList from "./src/scenes/product-list";
import ProductDetail from "./src/scenes/product-detail";
import FavoriteSpecDetail from './src/scenes/favorite-spec-detail';
import Intro from './src/scenes/introduction';

// Components
import SideMenu from "./src/components/side-menu";
import TabBar from "./src/components/tab-bar";
import IconButton from "./src/components/icon-button";

// Theme
import { colors } from "./src/theme";
import SpecDetail from './src/scenes/spec-detail';

import { Actions } from "react-native-router-flux";
// let socket = io('https://desolate-peak-95662.herokuapp.com', {
//   jsonp: false,
//   transports: ['websocket']
// });

// let socketIoMiddleware = createSocketIoMiddleware(socket, 'server/');

import { hook } from 'cavy';

const appStore = createStore(allReducers, {}, applyMiddleware(apiService, storageService));
// const appStore = applyMiddleware(apiService, storageService, socketService, socketIoMiddleware)(createStore)(allReducers);


class App extends Component {
  state = {}
  render() {
    return (
      <Provider store={appStore}>
        <Router >
          <Drawer
            key="Root"
            drawerPosition="right"
            headerMode="float"
            drawerWidth={200}
            contentComponent={SideMenu}
            titleStyle={{ color: colors.whiteB, fontFamily: 'Kanit-SemiBold', fontWeight: 'normal' }}
            renderRightButton={() =>
              <IconButton size="sm"
                onPress={() => Actions.drawerOpen()}
                imgPath={require('./src/assets/icons/hamburger-light.png')}
              />}
            navigationBarStyle={{ backgroundColor: colors.greenD }}
            backButtonTintColor={colors.whiteA}
            rightButtonTintColor={colors.whiteB}
          >
            <Stack key="Builder-stack">
              <Scene hideNavBar key="Intro" component={Intro} drawerLockMode="locked-closed" />

              <Tabs key="Builder-stack-tab" tabBarPosition="bottom"
                tabBarComponent={TabBar} childNames={['Auto', 'Manual']}>
                <Scene key="Auto" component={Auto} title="Builder" />
                <Scene key="Manual" component={Manual} title="Builder" />
              </Tabs>

              <Scene
                title="Products"
                drawerLockMode="locked-closed"
                renderRightButton={() => <View />}
                back={true} key="Product-list"
                component={ProductList}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />
              <Scene
                title="Detail"
                drawerLockMode="locked-closed"
                renderRightButton={() => <View />}
                back={true} key="Product-detail"
                component={ProductDetail}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />
              <Scene
                title="Spec Detail"
                drawerLockMode="locked-closed" renderRightButton={() => <View />}
                back={true} key="Spec-detail"
                component={SpecDetail}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />

            </Stack>

            <Stack key="Profile-stack">
              <Scene
                title="My Spec"
                key="My-spec"
                component={MySpec}
              />
              <Scene
                title="Spec Detail"
                drawerLockMode="locked-closed"
                renderRightButton={() => <View />}
                back={true}
                key="Favorite-spec-detail"
                component={FavoriteSpecDetail}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />
              <Scene
                title="Products"
                drawerLockMode="locked-closed"
                renderRightButton={() => <View />}
                back={true}
                key="Profile-product-list"
                component={ProductList}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />
              <Scene
                title="Detail"
                drawerLockMode="locked-closed"
                renderRightButton={() => <View />}
                back={true}
                key="Profile-product-detail"
                component={ProductDetail}
                renderBackButton={() =>
                  <IconButton onPress={() => { Actions.pop() }}
                    imgPath={require('./src/assets/icons/back.png')} />}
              />
            </Stack>

          </Drawer>
        </Router>
      </Provider>
    );
  }
}


// export default App;
const TestApp = hook(App);
export default TestApp;