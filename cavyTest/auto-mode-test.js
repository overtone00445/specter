
export default function(spec) {
  spec.describe('Auto mode', function() {
    spec.it('Input budget and Search', async function() {
      await spec.pause(2000);
      await spec.fillIn('Auto.TextInput', '25000');
      await spec.pause(2000);
      await spec.press('Auto.Search');
      await spec.pause(2000);
    });

    spec.it('Select spec and Save it', async function() {
      await spec.exists("recommendNo.1");
      await spec.press("recommendNo.1");
      await spec.pause(2000);
      await spec.exists("Spec.saveBtn");
      await spec.press("Spec.saveBtn");
      await spec.pause(2000);
      await spec.exists("Spec.textInput");
      await spec.fillIn("Spec.textInput", "Good");
      await spec.pause(2000);
      await spec.exists('Spec.textInput.confirmBtn');
      await spec.press('Spec.textInput.confirmBtn');
      await spec.pause(2000);
    });

  });
}
