
export default function(spec) {
  spec.describe('Manual mode', function() {
    
    spec.it('Go to Manual mode', async function() {
      await spec.exists('Auto');
      await spec.exists('Manual');
      await spec.pause(3000);
      await spec.press('Manual');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select CPU', async function() {
      await spec.exists('CPU');
      await spec.press('CPU');
      await spec.exists('INTEL 1151 CELERON G3930 2.9 GHz');
      await spec.exists('INTEL 1151 CELERON G3930 2.9 GHz.AddBtn');
      await spec.press('INTEL 1151 CELERON G3930 2.9 GHz.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Delete CPU peference box', async function() {
      await spec.exists('INTEL 1151 CELERON G3930 2.9 GHz.box');
      await spec.exists('INTEL 1151 CELERON G3930 2.9 GHz.deleteBtn');
      await spec.press('INTEL 1151 CELERON G3930 2.9 GHz.deleteBtn');
      await spec.pause(3000);
      await spec.exists('confirmDelete.btn');
      await spec.press('confirmDelete.btn');
      await spec.pause(3000);
    });

    spec.it('Select & Add new CPU', async function() {
      await spec.exists('CPU');
      await spec.press('CPU');
      await spec.exists('INTEL 1151 CORE I3-7100 3.9 GHz');
      await spec.exists('INTEL 1151 CORE I3-7100 3.9 GHz.AddBtn');
      await spec.press('INTEL 1151 CORE I3-7100 3.9 GHz.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select Mainboard', async function() {
      await spec.exists('Mainboard');
      await spec.press('Mainboard');
      await spec.exists('MAINBOARD 1151 MSI H270-A PRO');
      await spec.exists('MAINBOARD 1151 MSI H270-A PRO.AddBtn');
      await spec.press('MAINBOARD 1151 MSI H270-A PRO.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select GPU', async function() {
      await spec.exists('GPU');
      await spec.press('GPU');
      await spec.exists('VGA MSI GTX1030 AERO ITX 2G OC');
      await spec.exists('VGA MSI GTX1030 AERO ITX 2G OC.AddBtn');
      await spec.press('VGA MSI GTX1030 AERO ITX 2G OC.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select SSD', async function() {
      await spec.exists('SSD');
      await spec.press('SSD');
      await spec.exists('128 GB SSD ADATA SP600 SATA');
      await spec.exists('128 GB SSD ADATA SP600 SATA.AddBtn');
      await spec.press('128 GB SSD ADATA SP600 SATA.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select Harddisk', async function() {
      await spec.exists('Harddisk');
      await spec.press('Harddisk');
      await spec.exists('1.0 TB HDD SEAGATE SATA-3 BARRACUDA (ST1000DM010)');
      await spec.exists('1.0 TB HDD SEAGATE SATA-3 BARRACUDA (ST1000DM010).AddBtn');
      await spec.press('1.0 TB HDD SEAGATE SATA-3 BARRACUDA (ST1000DM010).AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select RAM', async function() {
      await spec.exists('RAM');
      await spec.press('RAM');
      await spec.exists('8 GB RAM PC DDR4/2133 APACER GRAY PANTHER LONG');
      await spec.exists('8 GB RAM PC DDR4/2133 APACER GRAY PANTHER LONG.AddBtn');
      await spec.press('8 GB RAM PC DDR4/2133 APACER GRAY PANTHER LONG.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select Power Supply', async function() {
      await spec.exists('Power Supply');
      await spec.press('Power Supply');
      await spec.exists('POWER SUPPLY MONSTER 550W (PM550W)');
      await spec.exists('POWER SUPPLY MONSTER 550W (PM550W).AddBtn');
      await spec.press('POWER SUPPLY MONSTER 550W (PM550W).AddBtn');
      await spec.pause(3000);
    });

    spec.it('Check avaliable & Select Monitor', async function() {
      await spec.exists('Monitor');
      await spec.press('Monitor');
      await spec.exists('LED MONITOR BENQ 20" TN GL2070');
      await spec.exists('LED MONITOR BENQ 20" TN GL2070.AddBtn');
      await spec.press('LED MONITOR BENQ 20" TN GL2070.AddBtn');
      await spec.pause(3000);
    });

    spec.it('Save the spec', async function() {
      await spec.exists('Save.btn');
      await spec.press('Save.btn');
      await spec.pause(3000);
      await spec.exists('Save.textInput');
      await spec.fillIn('Save.textInput','Nice');
      await spec.pause(1000);
      await spec.exists('Save.textInput.confirmBtn');
      await spec.press('Save.textInput.confirmBtn');
      await spec.pause(3000);
    });
    
  });
}
